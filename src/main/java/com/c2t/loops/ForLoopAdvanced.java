package com.c2t.loops;

public class ForLoopAdvanced {

	public static void main(String[] args) {
		
		int a [] = {10,20,40};
		
		for (int i : a) {
			System.out.println(i);
		}

	}

}
