package com.c2t.interview.appdirect.datastructures;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Node {
	int data;
	Node pointer;

	Node(int x) {
		data = x;
		pointer = null;
	}
}

public class LinkeListImpl {

	static Node head;

	static void displayLinkedList() {
		Node temp = head;

		while (temp != null) {
			int nodeVal = temp.data;
			System.out.println(nodeVal);
			temp = temp.pointer;
		}

		temp = null;
	}

	static void addingNodeAtBeginning() {
		Node node1 = new Node(0);
		node1.pointer = head;
		head = node1;
	}

	static void addingNodeAtEnd() {
		Node node = new Node(6);

		if (head.pointer == null) {
			head.pointer = node;
		} else {
			// Traverse the linkedList
			Node temp = head;

			while (temp.pointer != null) {
				temp = temp.pointer;
			}

			temp.pointer = node;

		}

	}

	static void deleteFromLinkedList(int val) {
		Node temp = head;
		
		while(temp!=null){
			
		}
	}
	
	static void createLinkedList() {

		// Creating node
		Node node1 = new Node(1);
		Node node2 = new Node(2);
		Node node3 = new Node(3);
		Node node4 = new Node(4);
		Node node5 = new Node(5);

		// Creating linked list

		node1.pointer = node2;
		node2.pointer = node3;
		node3.pointer = node4;
		node4.pointer = node5;
		node5.pointer = null;

		// Initializing header
		head = node1;
	}

	public static void main(String[] args) {
		createLinkedList();
		/*displayLinkedList();
		addingNodeAtBeginning();
		displayLinkedList();
		addingNodeAtEnd();
		displayLinkedList();*/
	}
}
