package com.c2t.variables;

class Student{
	String name;
	static String instructor;
	
	void display(){
		System.out.println("I am in display...");
		System.out.println(name);
		System.out.println(instructor);
	}
	
	static void study(){
		System.out.println("I am in display...");
		//System.out.println(name);
		System.out.println(instructor);
	}


}

public class JavaVariables {
	public static void main(String[] args) {
		Student s1 = new Student();
		s1.name = "AAA";
		s1.instructor = "Naresh";
		
		Student s2 = new Student();
		s2.name = "BBB";
		
		
		System.out.println(s1.instructor);
		System.out.println(s2.instructor);
		
		s2.instructor = "Naresh Selenium";
		
		System.out.println(Student.instructor);

	}
}
