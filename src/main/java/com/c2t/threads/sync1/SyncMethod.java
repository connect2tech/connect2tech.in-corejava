package com.c2t.threads.sync1;

class PrintTables {
	public void tables(int number) {

		synchronized (this) {
			for (int i = 1; i <= 10; i++) {

				System.out.println("I am number=" + number);

			}
		}
	}
}

class Thread1 extends Thread {

	PrintTables pt2;

	Thread1(PrintTables p) {
		pt2 = p;
	}

	public void run() {
		pt2.tables(5);
		// PrintTables.m1();
	}
}

class Thread2 extends Thread {

	PrintTables pt1;

	Thread2(PrintTables p) {
		pt1 = p;
	}

	public void run() {
		pt1.tables(10);
		// PrintTables.m1();
	}
}

public class SyncMethod {

	public static void main(String[] args) {
		PrintTables obj = new PrintTables();

		Thread1 t1 = new Thread1(obj);
		Thread2 t2 = new Thread2(obj);

		t1.start();
		t2.start();

	}

}
