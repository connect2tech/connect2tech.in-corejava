package com.c2t.functions;

public class CoffeeMachine2 {

	static void startMachine(){
		System.out.println("warm water");
		System.out.println("warm milk");
		System.out.println("Grind coffee beans..");
	}
	
	static String getCoffee(){
		System.out.println("add water");
		System.out.println("add milk");
		System.out.println("add sugar");
		return "coffee";
	}
	
	public static void main(String[] args) {
		startMachine();
		String mug1 = getCoffee();
		String mug2 = getCoffee();
		
		System.out.println(mug1);
		System.out.println(mug2);
	}

}
