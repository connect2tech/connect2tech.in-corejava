package com.c2t.functions;

public class PassByValue {

	static void sum(int x, int y){
		x = x + 10;
		y = y + 20;
		int summation = x + y;
		System.out.println("summation="+summation);
	}
	
	public static void main(String[] args) {

		int a = 10;
		int b = 20;
		
		System.out.println("before method call...");
		System.out.println("a="+a);
		System.out.println("b="+b);
		
		sum(a,b);
		
		System.out.println("after method call...");
		System.out.println("a="+a);
		System.out.println("b="+b);
	
	}
}
