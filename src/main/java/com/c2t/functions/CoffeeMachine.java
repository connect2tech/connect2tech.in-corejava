package com.c2t.functions;

public class CoffeeMachine {
	
	static void powerOnOffMachine(){
		System.out.println("Warm milk");
		System.out.println("Warm water");
		System.out.println("Grind coffee beans");
	}
	
	static String getCoffee(){
		System.out.println("Mix milk + water + sugar + beans");
		return "Coffee";
	}
	
	static String getCustomCoffee(int sugarAmount, int milkAmount){
		System.out.println("Mix milk + water + sugar + beans" + milkAmount + sugarAmount);
		return "Coffee";
	}
	
	public static void main(String[] args) {
		
		powerOnOffMachine();
		String mug1 = getCoffee();
		String mug2 = getCoffee();
		String mug3 = getCustomCoffee(10, 10);
		
		System.out.println(mug1);
		System.out.println(mug2);
	}
}
