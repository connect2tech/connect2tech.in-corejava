package com.c2t.functions;

public class FunctionCallStack {

	public static void m1() {
		String name = "Java";
		
		m2();
	}

	public static void m2() {
		m3();
	}

	public static void m3() {

	}

	public static void main(String[] args) {
		
		int a = 10;
		
		m1();
	}

}
