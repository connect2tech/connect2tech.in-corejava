package com.c2t.khalid6.oops;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Hiding_Super {
	int a = 10;
}

class Hiding_Sub extends Hiding_Super {
	static int a = 20;

	public void display() {
		System.out.println("a=" + a);
		System.out.println("super.a=" + super.a);

		Hiding_Super s = new Hiding_Sub();
		System.out.println("s.a=" + s.a);

	}
}

public class HidingMembers {
	public static void main(String[] args) {
		Hiding_Sub sub = new Hiding_Sub();
		sub.display();
	}
}
