package com.c2t.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

class FIGmdEmployees{
	String name;
	String empId;
}


public class HashMapExample2 {
	public static void main(String[] args) {

		HashMap<String, FIGmdEmployees> hm = new HashMap<String, FIGmdEmployees>();
		
		FIGmdEmployees e1 = new FIGmdEmployees();
		e1.name = "emp1";
		e1.empId = "100";
		
		FIGmdEmployees e2 = new FIGmdEmployees();
		e2.name = "emp1";
		e2.empId = "100";

		
		hm.put("id1", e1);
		hm.put("id2", e2);
		
		FIGmdEmployees emp = hm.get("id1");
		System.out.println(emp.empId);
		System.out.println(emp.name);

		

	}
}
