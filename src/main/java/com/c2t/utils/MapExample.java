package com.c2t.utils;

import java.util.*;


public class MapExample {
	public static void main(String[] args) {

		HashMap <String, String> h = new HashMap<String, String>();
		
		h.put("k1", "v1");
		h.put("k2", "v2");
		
		System.out.println(h);

		String val = h.get("k11");
		System.out.println(val);
		
	}
}
