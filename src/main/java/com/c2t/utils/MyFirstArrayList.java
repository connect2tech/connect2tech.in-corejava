package com.c2t.utils;

import java.util.ArrayList;
import java.util.Iterator;

public class MyFirstArrayList {
	public static void main(String[] args) {

		ArrayList <String> l = new ArrayList<String>();
		
		l.add("X");
		l.add("A");
		l.add("Y");
		l.add("X");
		
		System.out.println(l);
		
		boolean bool = l.contains("A1");
		System.out.println(bool);
		
		String s = l.get(2);
		System.out.println(s);
		
		/*System.out.println(l);
		l.clear();
		System.out.println(l);*/

		System.out.println("-----------------------------");

		/*for(int i=0;	i<l.size();	i++){
			String val = l.get(i);
			System.out.println(val);
		}*/
		
		
		Iterator <String> iter = l.iterator();

		while(iter.hasNext()){
			
			String s1 = iter.next();
			System.out.println(s1);
			
		}
	}
}
