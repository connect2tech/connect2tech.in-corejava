package com.c2t.utils.blogs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListIteration {
	public static void main(String[] args) {
		// create list
		List<String> topics = new ArrayList<String>();

		// add 4 different values to list
		topics.add("Java");
		topics.add("J2ee");
		topics.add("Selenium");
		topics.add("Python");

		// iterate via "for loop"
		System.out.println("###### For Loop Example.");
		for (int i = 0; i < topics.size(); i++) {
			System.out.println(topics.get(i));
		}

		// iterate via "New way to loop"
		System.out.println("\n###### Advance For Loop Example..");
		for (String temp : topics) {
			System.out.println(temp);
		}

		// iterate via "iterator loop"
		System.out.println("\n###### Iterator Example...");
		Iterator<String> iter = topics.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}

		// iterate via "while loop"
		System.out.println("\n###### While Loop Example #####");
		int i = 0;
		while (i < topics.size()) {
			System.out.println(topics.get(i));
			i++;
		}

		// collection stream() util: Returns a sequential Stream with this
		// collection as its source
		System.out.println("\n###### collection stream() util #####");
		topics.forEach((temp) -> {
			System.out.println(temp);
		});
	}
}
