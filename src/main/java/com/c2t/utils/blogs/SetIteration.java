package com.c2t.utils.blogs;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetIteration {
	public static void main(String[] args) {
		// Creating and initializing an HashSet for iteration
		Set<String> setOfStocks = new HashSet<>();
		setOfStocks.add("INFY");
		setOfStocks.add("BABA");
		setOfStocks.add("GOOG");
		setOfStocks.add("MSFT");
		setOfStocks.add("AMZN");
		System.out.println("Set: " + setOfStocks);

		System.out.println("###### Looping over HashSet using advanced forloop ######");
		for (String stock : setOfStocks) {
			System.out.println(stock);
		}

		Iterator<String> itr = setOfStocks.iterator();
		// traversing over HashSet
		System.out.println("###### Traversing over Set using Iterator ######");
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
	}
}
