package com.c2t.utils;

import java.util.HashMap;

public class HashMapWithNull {
	public static void main(String[] args) {
		HashMap<String, String> m = new HashMap<String, String>();
		
		m.put("location", null);
		m.put("location2", null);
		m.put(null, null);
		
		System.out.println(m);
	}
}
