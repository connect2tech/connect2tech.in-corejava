package com.c2t.scjp7.resources;

class Auto1 implements AutoCloseable {

	@Override
	public void close() throws Exception {
		System.out.println("Auto1");
	}

}

class Auto2 implements AutoCloseable {

	@Override
	public void close() throws Exception {
		System.out.println("Auto2");
	}

}

public class Java7ResourceManagement3 {

	public static void main(String[] args) {
		try (Auto1 a1 = new Auto1(); Auto2 a2 = new Auto2()) {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}