package com.c2t.scjp.oops;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Test {

	public static String foo() {
		System.out.println("Test foo called");
		return "";
	}

	public static void main(String args[]) {
		Test obj = null;
		System.out.println(obj.foo());
	}

}
