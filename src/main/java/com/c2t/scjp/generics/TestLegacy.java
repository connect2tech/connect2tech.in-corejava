package com.c2t.scjp.generics;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import java.util.*;

class Inserter {
	// method with a non-generic List argument
	void insert(List list) {
		list.add(new Integer(42)); // adds to the incoming list
		list.add(new String("42"));
	}
}

class Adder {
	int addAll(List list) {
		// method with a non-generic List argument,
		// but assumes (with no guarantee) that it will be Integers
		Iterator it = list.iterator();
		int total = 0;
		while (it.hasNext()) {
			int i = ((Integer) it.next()).intValue();
			total += i;
		}
		return total;
	}
}

public class TestLegacy {
	public static void main(String[] args) {
		List<Integer> myList = new ArrayList<Integer>();
		// type safe collection
		myList.add(4);
		myList.add(6);
		Adder adder = new Adder();
		int total = adder.addAll(myList);
		// pass it to an untyped argument
		System.out.println(total);
	}
}
