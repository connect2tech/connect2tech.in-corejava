package com.c2t.scjp.lang;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class UsingSystem2 {
	public static void main(String[] args) {
		Map<String, String> map = System.getenv();
		System.out.println(map);
		
		Set <String> set = map.keySet();
		
		Iterator <String> iter = set.iterator();

		while(iter.hasNext()){
			String key = iter.next();
			System.out.print(key+ " = " );
			System.out.println(map.get(key));
		}
		
	}
}
