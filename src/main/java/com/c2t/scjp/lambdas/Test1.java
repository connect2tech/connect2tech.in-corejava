package com.c2t.scjp.lambdas;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

interface FuncInterface 
{ 
    public abstract void multiplication(int x);
} 
  
public class Test1 
{ 
    public static void main(String args[]) 
    { 
    	FuncInterface f = (int y)->System.out.println(2*y);
    	
    	f.multiplication(10);
    } 
}