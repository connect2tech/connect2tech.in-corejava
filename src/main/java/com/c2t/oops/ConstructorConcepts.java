package com.c2t.oops;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Mobile{
	
	String manufacturer;
	
	Mobile(){
		System.out.println("i am mobile");
		manufacturer = "china";
	}
	
	Mobile(String m){
		System.out.println("i am mobile");
		manufacturer = m;
	}
}



public class ConstructorConcepts {
	public static void main(String[] args) {
	
		Mobile m = new Mobile();
		System.out.println(m.manufacturer);
		
		Mobile m6 = new Mobile("taiwan");
		System.out.println(m6.manufacturer);
		
	}
}
