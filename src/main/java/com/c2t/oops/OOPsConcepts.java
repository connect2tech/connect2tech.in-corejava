package com.c2t.oops;


class StudentFIGmd{
	
	int empId;
	String name;
	
	public void attendTraining(){
		System.out.println("attendTraining");
		
		System.out.println("this="+this);
		
		System.out.println(this.empId);
		System.out.println(name);
	}
	
}

public class OOPsConcepts {
	public static void main(String[] args) {
		int a = 10;
		
		StudentFIGmd s = new StudentFIGmd();
		
		s.name = "AAA";
		s.empId = 100;
		
		System.out.println(s.name);
		System.out.println(s.empId);
		
		System.out.println("s="+s);
		
		s.attendTraining();
		
	}
}
