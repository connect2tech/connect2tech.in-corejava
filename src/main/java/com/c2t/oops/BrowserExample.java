package com.c2t.oops;

class Browser {
	public void openBrowser() {
		System.out.println("Browser..");
	}
}
class Chrome extends Browser {
	public void openBrowser() {
		System.out.println("Chrome..");
	}
}
class FF extends Browser {
	public void openBrowser() {
		System.out.println("FF..");
	}
}
public class BrowserExample {
	public static void main(String[] args) {
		Browser b = null;
		int client = 5;

		if (client == 0) {
			b = new FF();
		} else{
			b = new Chrome();
		}
		b.openBrowser();
	}
}
