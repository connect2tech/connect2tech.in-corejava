package com.c2t.oops;

class Emp {

	String name;

	public String toString() {

		return name;

	}

}

public class Objects {
	public static void main(String[] args) {

		Emp e1 = new Emp();
		e1.name = "chandan";

		Emp e2 = new Emp();
		e2.name = "ramana";

		System.out.println(e1.toString());
		System.out.println(e2.toString());
	}
}
