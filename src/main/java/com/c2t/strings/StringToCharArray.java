package com.c2t.strings;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class StringToCharArray {
	public static void main(String[] args) {
		String s = "Naresh Chaurasia";
		
		char c[] = {'a','b'};

		char cArr[] = s.toCharArray();
		System.out.println(cArr);
		System.out.println(c);
		
	}
}
