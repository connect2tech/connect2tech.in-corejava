package com.c2t.strings;

public class StringInJava {
	public static void main(String[] args) {

		String name = "SaMeer";

		String name2 = new String("Java J2ee");

		// charAt(int index)
		// equals(String anotherString)
		// concat(String str)

		char c = name.charAt(5);

		System.out.println("c=" + c);

		boolean bool = name.equals(name2);

		System.out.println("bool=" + bool);

		String finalVal = name.concat(" Deshmukh");

		System.out.println("finalVal=" + finalVal);

	}
}
