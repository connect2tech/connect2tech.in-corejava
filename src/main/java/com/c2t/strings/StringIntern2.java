package com.c2t.strings;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class StringIntern2 {
	public static void main(String[] args) {

		String s1 = new String("abc"); // goes to Heap Memory, like other
										// objects
		String s2 = "abc"; // goes to String Pool
		String s3 = "abc"; // again, goes to String Pool

		// Let's check out above theories by checking references
		System.out.println("s1==s2? " + (s1 == s2)); // should be false
		System.out.println("s2==s3? " + (s2 == s3)); // should be true

		// Let's call intern() method on s1 now
		String s5 = s1.intern(); // this should return the String with same value, BUT
							// from String Pool

		// Let's run the test again
		System.out.println("s1==s5? " + (s1 == s5)); // should be true now
	}
}
