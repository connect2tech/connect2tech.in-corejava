package com.c2t.ist1130;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class SetExample {
	public static void main(String[] args) {
		Set<String> s = new HashSet<String>();
		
		s.add("a");
		s.add("b");
		s.add("a");
		
		System.out.println(s);
		
		Iterator <String> iter = s.iterator();
		
		while(iter.hasNext()){
			String val = iter.next();
			System.out.println(val);
		}

	}
}
