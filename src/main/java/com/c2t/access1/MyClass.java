package com.c2t.access1;

public class MyClass{
	
	void privateMethod(){
		System.out.println("private");
	}
	
	
	public void display(){
		
		privateMethod();
		
		System.out.println("I am public method...");
	}
}
