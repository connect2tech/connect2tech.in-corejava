package com.c2t.java8.defaultmethod;

public class ConcreteClass implements Java8Interface, Java8Interface_1 {
	public static void main(String[] args) {
		ConcreteClass c = new ConcreteClass();
		c.hi();
		Java8Interface.hello();
	}

	// We need to override hi method otherwise compilation error
	@Override
	public void hi() {
		Java8Interface.super.hi();
	}
}