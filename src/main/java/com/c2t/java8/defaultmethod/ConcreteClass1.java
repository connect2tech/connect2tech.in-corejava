package com.c2t.java8.defaultmethod;

public class ConcreteClass1 implements Java8Interface {
    public static void main(String[] args) {
        ConcreteClass1 c = new ConcreteClass1();
        c.hi(); // would be accessible using class instance
        //Not possible to call using class instance
        Java8Interface.hello();
    }
}
