package com.c2t.ist830;

interface Remote {
	
	String color = "red";
	
	public void powerOnOff();

	public void changeVolume();
}

class TvRemote implements Remote {
	public void powerOnOff() {
		System.out.println("Power on ....");
	}

	public void changeVolume() {
		System.out.println("Change volume ....");
	}
}

public class InterfaceConcepts {
	public static void main(String[] args) {
		Remote r = new TvRemote();
		r.changeVolume();
	}
}
