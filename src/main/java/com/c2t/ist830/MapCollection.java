package com.c2t.ist830;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class MapCollection {
	public static void main(String[] args) {
		HashMap<String, String> map = new HashMap<String, String>();
		
		map.put("k1", "v1");
		map.put("k2", "v2");
		
		System.out.println(map);
		
		//get(Object key)
		//String val = map.get("k11");
		//System.out.println(val);
		
		Set <String> set = map.keySet();
		
		Iterator <String>iter = set.iterator();
		while(iter.hasNext()){
			String key = iter.next();
					
			String value = map.get(key);
			
			System.out.println(key);
			System.out.println(value);
			
			System.out.println("----------------");
		}
		
	}
}
