package com.c2t.ist830;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class NestedExceptionHandling {

	public static void main(String[] args) {

		try {

			try {

			} catch (Exception e2) {

			}
			
			try {

			} catch (Exception e2) {

			}
			
			try {

			} catch (Exception e2) {

			}

			// db operations
		} catch (Exception e) {
			// close connection.

			try {

			} catch (Exception e2) {

			}
		}

	}

}
