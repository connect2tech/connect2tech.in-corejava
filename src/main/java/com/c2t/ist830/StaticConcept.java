package com.c2t.ist830;

class Student{
	String name;
	static String instructor;
	
	public void display_instance(){
		System.out.println("I am display method...");
		System.out.println(name);
		System.out.println(instructor);
	}
	
	public static void display_static(){
		System.out.println("I am display method...");
		//System.out.println(name);
		System.out.println(instructor);
	}
}


public class StaticConcept {
	public static void main(String[] args) {
		Student s1 = new Student();
		s1.name = "user1";
		s1.instructor = "Naresh";

	
		Student s2 = new Student();
		s2.name = "user2";
		System.out.println(s2.instructor);
		
		s2.instructor = "nc";
		System.out.println(s1.instructor);
		
		System.out.println(Student.instructor);

	}
}
