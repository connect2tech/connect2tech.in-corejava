package com.c2t.innerclasses;

class Popcorn {
	public void pop() {
		System.out.println("popcorn");
	}
}

public class Anonymous {
	public static void main(String[] args) {
		Popcorn p = new Popcorn() {
			public void pop() {
				System.out.println("anonymous popcorn");
			}
		};
		
		p.pop();
	}
}
