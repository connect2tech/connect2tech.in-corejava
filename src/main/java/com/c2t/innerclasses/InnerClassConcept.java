package com.c2t.innerclasses;

class Outer{
	
	private int a = 10;
	
	class Inner{
		
		public void display(){
			System.out.println("I am inside the inner class..."+a);
		}
		
	}
	
	public void outerDisplay(){
		Inner i = new Inner();
		i.display();
	}
	
}


public class InnerClassConcept {
	public static void main(String[] args) {
		Outer outer = new Outer();
		outer.outerDisplay();
		
		/*Outer.Inner inner = outer.new Inner();
		inner.display();*/
	}
}
