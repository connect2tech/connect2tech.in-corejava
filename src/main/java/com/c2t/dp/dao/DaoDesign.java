package com.c2t.dp.dao;

import java.util.List;

interface EmployeeDao {
	public void insertEmployee();

	public void updateEmployee();

	public void deleteEmployee();

	public Employee getEmployee();

	public List<Employee> getEmployees();
}

class EmployeeDaoJdbcImpl implements EmployeeDao {

	@Override
	public void insertEmployee() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateEmployee() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteEmployee() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Employee getEmployee() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Employee> getEmployees() {
		// TODO Auto-generated method stub
		return null;
	}
	
}

class EmployeeDaoHbmImpl implements EmployeeDao {

	@Override
	public void insertEmployee() {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateEmployee() {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteEmployee() {
		// TODO Auto-generated method stub

	}

	@Override
	public Employee getEmployee() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Employee> getEmployees() {
		// TODO Auto-generated method stub
		return null;
	}

}

public class DaoDesign {
	public static void main(String[] args) {

	}
}
