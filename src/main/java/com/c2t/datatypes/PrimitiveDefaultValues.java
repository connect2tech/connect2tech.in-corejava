package com.c2t.datatypes;

public class PrimitiveDefaultValues {
	
	static int a;
	static boolean b;
	static char c;
	static float f;
	
	//static char d = '\u0000';
	
	public static void main(String[] args) {
		
		char cArr [] = new char[5];
		int aArr [] = new int[5];
		
		System.out.println("global a = "+a);
		System.out.println("global a = "+(int)a);
		
		System.out.println("global b = "+b);
		
		System.out.println("global c = "+c);
		System.out.println("global c = "+(int)c);
		
	}
	
	static void anotheMethod(){
		
	}
}
