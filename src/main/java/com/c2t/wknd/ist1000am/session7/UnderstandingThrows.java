package com.c2t.wknd.ist1000am.session7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class UnderstandingThrows {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		readData();
	}

	public static void readData() throws FileNotFoundException, IOException {
		readFromFile();
	}

	public static void readFromFile() throws FileNotFoundException, IOException {
		File f = new File("D:/Samp.txt");
		FileInputStream fis = new FileInputStream(f);

	}
}
