package com.c2t.wknd.ist1000am.session7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class UnderstandingThrow {

	public static void main(String[] args) {

			int age = 15;
			
			if(age>=18){
				System.out.println("You can cast your vote...");
			}else{
				throw new RuntimeException("You are below 18 Years!!!");
			}
			
			System.out.println("------------------------");
	
	}

}
