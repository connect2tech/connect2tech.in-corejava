package com.c2t.wknd.ist1000am.session7;

import com.c2t.exception.TryCatch1;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class NestedExceptions {
	public static void main(String[] args) {
		try {
			try {
				
			} catch (Exception e) {
				// TODO: handle exception
			}
		} catch (Exception e) {
			// TODO: handle exception
			try {
				
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
	}
}
