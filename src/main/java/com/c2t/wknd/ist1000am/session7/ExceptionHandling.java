package com.c2t.wknd.ist1000am.session7;

public class ExceptionHandling {
	public static void main(String[] args) {

		int a = 0;
		int b = 20;

		int c = 0;

		int aa[] = { 10, 20 };

		System.out.println("value of c1=" + c);

		try {

			System.out.println(aa[10]);

			c = b / a;
			System.out.println("after division...");
		} catch (ArrayIndexOutOfBoundsException e2) {
			System.out.println(e2);
		}finally{
			System.out.println("i am in finally..");
		}

		System.out.println("value of c2=" + c);

	}
}
