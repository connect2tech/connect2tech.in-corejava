package com.c2t.wknd.ist1000am.session3;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ArraysInJava2d {
	public static void main(String[] args) {
		int matrix1[][] = {

				{ 10, 20 }, { 30, 40 }

		};

		for (int i = 0; i < 2; i++) {

			for (int j = 0; j < 2; j++) {
				matrix1[i][j] = i + j + 10;

				System.out.println(matrix1[i][j]);
			}
		}

	}
}
