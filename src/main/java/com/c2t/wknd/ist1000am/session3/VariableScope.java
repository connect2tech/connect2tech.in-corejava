package com.c2t.wknd.ist1000am.session3;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class VariableScope {
	
	static int a = 10;
	
	static void m1(){
		int b = 20;
		
		System.out.println(b);
		System.out.println(a);
	}
	
	
	static void m2(){
		int c = 30;
		
		System.out.println(c);
		System.out.println(a);
		//System.out.println(b);
	}
	
	public static void main(String[] args) {
		m1();
		m2();
	}
	
	
}
