package com.c2t.wknd.ist1000am.session3;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class CoffeeMachine {

	static void startMachine() {

		System.out.println("Warm water");
		System.out.println("Warm milk");
		System.out.println("Grind coffee beans");
	}

	static String getCoffee() {
		System.out.println("mix water");
		System.out.println("mix milk");
		System.out.println("mix sugar");
		System.out.println("mix coffee");

		return "coffee";
	}

	static String getCustomCoffee(int milk, int sugar, int coffee) {

		System.out.println("adding ");
		System.out.println("milk=" + milk);
		System.out.println("sugar = " + sugar);
		System.out.println("coffee =" + coffee);

		return "customizedCoffee";
	}

	public static void main(String[] args) {
		startMachine();
		String mug1 = getCoffee();
		String mug2 = getCoffee();
		String mug3 = getCoffee();

		getCustomCoffee(10, 10, 10);

		System.out.println(mug1);
	}
}
