package com.c2t.wknd.ist1000am.session3;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ArraysInJava {
	public static void main(String[] args) {
		int [] arr  = new int[5];
		
		int  arr2 []  = new int[5];
		
		int  arr3 []  = {10,20,30,40,50};
		
/*		
		for(int i=0;	i<5;	i++){
			arr[i] = i + 10;
			
			System.out.println(arr[i]);
		}*/
		
		for(int j=0;	j<5;	j++){
			System.out.println(arr3[j]);
		}
		
	}
}
