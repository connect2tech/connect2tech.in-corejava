package com.c2t.wknd.ist1000am.session6.pack2;

import com.c2t.wknd.ist1000am.session6.pack1.File1;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Account{
	private int balance=100000;
	
	int amount = 200;
	
	public void accessBalance(){
		System.out.println(balance);
	}
	
}

public class File2 {
	public static void main(String[] args) {
		Account a = new Account();
		a.accessBalance();
	}
}
