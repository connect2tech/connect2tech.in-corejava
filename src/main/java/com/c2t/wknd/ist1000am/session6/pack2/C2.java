package com.c2t.wknd.ist1000am.session6.pack2;

import com.c2t.wknd.ist1000am.session6.pack1.C1;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */




public class C2 extends C1{
	public static void main(String[] args) {
		C2 c2 = new C2();
		c2.method_proc();
		
/*		C1 c1 = new C1();
		c1.method_proc();*/
	}
}
