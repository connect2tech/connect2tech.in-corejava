package com.c2t.wknd.ist1000am.session6;

interface I1{
	
}
interface I11{
	
}

interface I2 extends I1, I11{
	
}

interface Bank {
	int loanPercent = 10;
	abstract public void debit();
	public void credit();
}

class HDFC implements Bank,I1 {

	public void debit() {
		
		//loanPercent = 12;
		System.out.println("Debit");
	}

	public void credit() {
		System.out.println("Credit");
	}
	
	public void giveLoan() {
		
	}

}

class ICICI implements Bank {

	public void debit() {
		
		//loanPercent = 12;
		System.out.println("Debit");
	}

	public void credit() {
		System.out.println("Credit");
	}
	
	public void giveLoan() {
		
	}

}
public class InterfaceConcepts {
	public static void main(String[] args) {
		Bank b = null;
		int a = 10;
		
		if(a>5) {
			b = new HDFC();
		}else {
			b = new ICICI();
		}
		b.debit();
		
		
	}
}
