package com.c2t.wknd.ist1000am.session6.pack2;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class File22 {
	public static void main(String[] args) {
		Account a = new Account();
		System.out.println(a.amount);
	}
}
