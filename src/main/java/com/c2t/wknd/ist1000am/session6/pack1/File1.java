package com.c2t.wknd.ist1000am.session6.pack1;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class File1 {
	public void method1(){
		System.out.println("method1");
	}
	
	void method2(){
		System.out.println("method2");
	}
}
