package com.c2t.wknd.ist1000am.session4;

public class StringManipulation {
	public static void main(String[] args) {

		// charAt(int index)
		// compareTo(String anotherString)
		// concat(String str)
		// contains(CharSequence s)
		// equals(Object anObject)
		
		String s1 = "hello";
		String s2 = "hai";
		String s3 = "hello";
		
		char c = s1.charAt(0);
		System.out.println(c);
		
		String ss = s1.concat(s2);
		System.out.println(ss);
		
		boolean b = s1.contains("llo");
		System.out.println(b);
		
		boolean b2 = s1.equals(s2);
		System.out.println(b2);
				

	}
}
