package com.c2t.wknd.ist1000am.session4;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Student {
	String name;
	
	void method(Student x){ //Student x = s1
		
		System.out.println(x.name);
		
		x.name = "BB";
		
	}
	
}

public class PassByReference {
	public static void main(String[] args) {
		Student s1 = new Student();
		s1.name = "AA";
		
		System.out.println(s1.name);
		
		s1.method(s1);
		
		System.out.println(s1.name);
	}
}
