package com.c2t.wknd.ist1000am.session4;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

class Person {
	String name;
	int age;

	void study() {
		System.out.println("I can study...");
	}
}
public class ClassObjectConcept {

	public static void main(String[] args) {
		
		
		
		Person p = new Person();
		p.name = "A";
		p.age = 10;
		
		Person p2 = p;
		
		System.out.println(p.name);
		System.out.println(p.age);
		
		p.study();
		
		
		
	}

}
