package com.c2t.wknd.ist1000am.session4;

class Student1 {
	String name;
	static String trainer;

	public void display() {
		System.out.println(name);
		System.out.println(trainer);
	}
	
	public static void display_static(){
		//System.out.println(name);
		System.out.println(trainer);
	}
}

public class StaticConcept {
	public static void main(String[] args) {

		Student1 s2 = new Student1();
		s2.name = "selenium";
		s2.display();
		//s2.trainer = "NC";
		Student1.trainer = "nc";

		
		Student1 s = new Student1();
		s.name = "java";
		s.display();
		//System.out.println(s.trainer);
		
		System.out.println(Student1.trainer);
		
		s.display();
		s.display_static();
	
	}
}
