package com.c2t.wknd.ist1000am.session4;

class Car{
	String color;
	final int fuelCapacity = 25;
	
	final public void startEngine(){
		System.out.println("startEngine...");
	}
}

class Maruti extends Car{
	/*public void startEngine(){
		System.out.println("startEngine of maruti...");
	}*/
}


public class FinalConcept {
	public static void main(String[] args) {
		Car c = new Car();
		System.out.println(c.color);
		
		c.color = "black";
		System.out.println(c.color);
		
		//c.fuelCapacity = 30;
	}
}
