package com.c2t.wknd.ist1000am.session4;

public class Calculator {

	static void sum(int x, int y) {
		System.out.println(x + y);
	}
	
	static void sum(String x, String y) {
		System.out.println(x + y);
	}
	
	static void sum(int x, int y, int z) {
		System.out.println(x + y + z);
	}

	public static void main(String[] args) {
		sum(10, 20);
		sum(10, 20, 30);
		sum("java"," selenium");

	}
}
