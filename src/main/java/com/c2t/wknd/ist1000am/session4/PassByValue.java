package com.c2t.wknd.ist1000am.session4;

public class PassByValue {
	static void add(int x, int y) {
		x = x + 10;
		y = y + 20;

		System.out.println("x=" + x);
		System.out.println("y=" + y);
		System.out.println("--------------------");
	}

	public static void main(String[] args) {
		int a = 10;
		int b = 20;
		
		System.out.println("a=" + a);
		System.out.println("b=" + b);
		System.out.println("--------------------");
	
		add(a, b);

		System.out.println("a=" + a);
		System.out.println("b=" + b);
	}

}
