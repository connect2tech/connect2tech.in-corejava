package com.c2t.wknd.ist1000am.session4;

abstract class Shape {
	abstract void draw();
	
	void color(){
		System.out.println("red");
	}
}

class Circle extends Shape {
	void draw() {
		System.out.println("Circle...");
	}
	
	void color(){
		System.out.println("yellow");
	}
}

public class AbstractConcept {
	public static void main(String[] args) {

	}
}
