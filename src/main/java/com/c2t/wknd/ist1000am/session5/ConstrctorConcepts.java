package com.c2t.wknd.ist1000am.session5;

class Remote{
	String manufacturer;
	
	Remote(){
		manufacturer = "china";
	}
	
	Remote(String manufacturer){
		this.manufacturer = manufacturer;
	}
	

}

public class ConstrctorConcepts {
	public static void main(String[] args) {
		Remote r = new Remote();
		System.out.println(r.manufacturer);
		
		Remote r2 = new Remote();
		System.out.println(r2.manufacturer);
		
		Remote r3 = new Remote("India");
		System.out.println(r3.manufacturer);
		
	}
}
