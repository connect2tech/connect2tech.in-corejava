package com.c2t.wknd.ist1000am.session5;

class Tv {
	int a = 10;
	int b;
	double d;
	String s;

}

public class DefaultValues {

	public static void main(String[] args) {
		Tv t = new Tv();
		System.out.println(t.a);
		System.out.println(t.b);
		System.out.println(t.s);
		System.out.println(t.d);
	}

}
