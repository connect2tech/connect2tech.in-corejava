package com.c2t.wknd.ist1000am.session5;

class Bike{
	String color;
	
	public void display(){
		System.out.println(color);
		System.out.println("this="+this);
		System.out.println("this.color="+this.color);
		
		this.color = "modified color";
	}
}

public class ThisKeyWord {
	public static void main(String[] args) {
		Bike b = new Bike();
		b.color = "black";
		
		b.display();
		
		System.out.println("b="+b);
		
		System.out.println(b.color);
	}
}
