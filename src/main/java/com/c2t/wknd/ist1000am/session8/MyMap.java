package com.c2t.wknd.ist1000am.session8;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class MyMap {
	public static void main(String[] args) {
		Map<String, String> m = new HashMap<String, String>();
		m.put("k1", "v1");
		m.put("k2", "v1");
		m.put("k2", "v2");
		System.out.println(m);
		
		String val = m.get("k1");
		System.out.println(val);
		
		Set <String> keys = m.keySet();
		
		System.out.println("------------");
		
		Iterator <String> iter = keys.iterator();
		while(iter.hasNext()){
			String key = iter.next();
			
			String value = m.get(key);
			System.out.println(value);
		}
		
	}
}
