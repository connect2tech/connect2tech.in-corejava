package com.c2t.wknd.ist1000am.session8;

import java.util.ArrayList;

public class MyArrayList {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();

		list.add("A");
		list.add("B");
		list.add("C");
		list.add("A");

		// add(E e)
		// clear()
		// get(int index)
		// indexOf(Object o)

		System.out.println(list);

		String s = list.get(0);
		System.out.println(s);

		int index = list.indexOf("C");
		System.out.println(index);
		
		list.clear();
		System.out.println(list);

	}
}
