package com.c2t.wknd.ist1000am.session8;

class MyThread2 implements Runnable{
	public void run() {
		System.out.println("new thread..");
		System.out.println("id2=" + Thread.currentThread().getId());
	}
}

public class UsingImplements {
	public static void main(String[] args) {
		System.out.println("id1=" + Thread.currentThread().getId());
		
		MyThread2 t = new MyThread2();
		
		Thread thread = new Thread(t);
		thread.start();
		
	}
}
