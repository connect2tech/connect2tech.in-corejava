package com.c2t.wknd.ist1000am.session8;

class MyThread extends Thread {

	public void run() {

		for (int i = 0; i < 5; i++) {

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("I am in run method...");
			System.out.println("id2=" + Thread.currentThread().getId());
		}
	}
}

public class Threading {
	public static void main(String[] args) {
		MyThread t1 = new MyThread();
		// t1.run();
		t1.start();

		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("i am in main");
			System.out.println("id1=" + Thread.currentThread().getId());
		}

	}
}
