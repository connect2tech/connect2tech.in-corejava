package com.c2t.wknd.ist1000am.session8;

import java.util.HashSet;
import java.util.Set;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class MySet {
	public static void main(String[] args) {
		
		Set <String> s = new HashSet<String>();
		s.add("A");
		s.add("B");
		s.add("A");
		
		System.out.println(s);
		
	}
}
