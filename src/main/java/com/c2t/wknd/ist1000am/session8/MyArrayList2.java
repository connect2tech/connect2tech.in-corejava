package com.c2t.wknd.ist1000am.session8;

import java.util.ArrayList;
import java.util.Iterator;

public class MyArrayList2 {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();

		list.add("A");
		list.add("B");
		list.add("C");
		list.add("A");

		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		System.out.println("------------------------");
		
		Iterator<String> iter = list.iterator();
		
		while(iter.hasNext()){
			String s = iter.next();
			System.out.println(s);
		}

	}
}
