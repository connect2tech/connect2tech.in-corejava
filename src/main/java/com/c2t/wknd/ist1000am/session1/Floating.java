package com.c2t.wknd.ist1000am.session1;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Floating {
	public static void main(String[] args) {
		
		double d1 = 10.5;
		double d2 = 20.5;
		
		float f = 12.1f;
		
	}
}
