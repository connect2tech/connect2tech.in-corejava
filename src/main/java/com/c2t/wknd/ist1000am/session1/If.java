package com.c2t.wknd.ist1000am.session1;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class If {
	public static void main(String[] args) {
		int a = 10;
		int b = 20;
		
		boolean bool = a>b;
		
		if(bool){
			System.out.println("a is greater than b");
		}else{
			System.out.println("a is less than b");
		}
	}
}
