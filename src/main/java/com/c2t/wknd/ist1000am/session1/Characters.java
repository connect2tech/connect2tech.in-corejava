package com.c2t.wknd.ist1000am.session1;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Characters {
	public static void main(String[] args) {
		int d = 'x';
		
		char c = 'A';
		System.out.println(c);
		System.out.println((int)c);
	}
}
