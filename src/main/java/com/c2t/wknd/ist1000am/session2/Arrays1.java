package com.c2t.wknd.ist1000am.session2;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Arrays1 {
	public static void main(String[] args) {
		int a [] = new int[5];

		for(int i=0;i<5;i++){
			a[i] = i;
			System.out.println(a[i]);
		}
		
		System.out.println(a[6]);
		
	}
}
