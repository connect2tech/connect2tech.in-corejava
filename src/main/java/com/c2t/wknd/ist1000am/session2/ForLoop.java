package com.c2t.wknd.ist1000am.session2;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ForLoop {
	public static void main(String[] args) {
		for(int i=0;	i<5;	i++){
			System.out.println("Hello World, welcome to java selenium...");
		}
	}
}
