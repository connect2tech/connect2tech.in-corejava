package com.c2t.primitives;

public class Unary {
	public static void main(String[] args) {
		int a = 10;
		int d = 0;
		
		d = a++; // a = a+1;
		System.out.println(a);
		System.out.println(d);
	}
}
