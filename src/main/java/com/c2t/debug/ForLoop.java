package com.c2t.debug;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ForLoop {
	
	public static void main(String[] args) {
		
		System.out.println("aaa");
		
		System.out.println("bbb");
		
		System.out.println("ccc");
		
	}

}
