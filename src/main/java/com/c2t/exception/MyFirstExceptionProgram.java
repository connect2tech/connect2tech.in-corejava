package com.c2t.exception;

public class MyFirstExceptionProgram {

	public static void main(String[] args) {

		int a = 0;
		int b = 20;
		int c = 0;

		int arr[] = { 10, 20 };

		try {
			c = b / a;
			System.out.println(arr[2]);
		} 
		finally{
			System.out.println("i am finally block...");
		}

		System.out.println("c=" + c);

	}
}
