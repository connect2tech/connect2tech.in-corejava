package com.c2t.exception;

import java.io.*;

public class TestThrows2 {

	public static void main(String[] args) {

		
		try {
			checkBalance();
			System.out.println("After checkBalance...");	
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Inside exception");
		}
		

	}

	public static void checkBalance() throws Exception {

		int balance = 0;

		if (balance <= 0) {
			// System.out.println("insufficient balance...");
			throw new Exception("Alert. Insufficient balance !!!");
		} else {
			System.out.println("Welcome to menu...");
		}

		System.out.println("Done with the code above...");

	}

}