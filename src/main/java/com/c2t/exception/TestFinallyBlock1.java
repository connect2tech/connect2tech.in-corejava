package com.c2t.exception;

class TestFinallyBlock1 {
	public static void main(String args[]) {

		int b = 0;
		
		int a = 10 / b;
		
		System.out.println(a);

		System.out.println("after finally");

		System.out.println("rest of the code...");
	}
}