package com.c2t.exception;

public class ThrowsExample {
	public static void main(String[] args) {

		int age = 15;

		try {
			if (age >= 18) {
				System.out.println("You can cast your vote..");
			} else {
				throw new RuntimeException("You are below voting age limit!!!");
			}
		} catch (RuntimeException e) {

		}

		System.out.println("After exception handling...");

	}

}
