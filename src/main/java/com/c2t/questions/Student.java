package com.c2t.questions;


public class Student extends Thread {
	Student str = null;

	public Student() {
	}

	Student(Student str) {
		this.str = str;
	}

	public synchronized void method() {
		for (int i = 0; i < 5; i++) {
			System.out.println(i);
			System.out.println("Thread::"+Thread.currentThread().getId());
		}
	}

	public void run() {
		str.method();
	}

	public static void main(String[] args) {
		Student m = new Student();
		Student st = new Student(m);
		Student k = new Student(st);
		st.start();
		k.start();
	}
}