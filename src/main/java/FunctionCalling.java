
/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class FunctionCalling {
	
	static void f2(){
		
		String s = "I am function f2";
		
	}
	
	static void f1(){
		int b = 20;
		f2();
	}
	
	public static void main(String[] args) {
		
		int a = 10;
		String name = "java";
		
		f1();
		
	}

}
