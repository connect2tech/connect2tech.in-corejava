package in.connect2tech.java5.generics;

class Milk {

}

class Water {

}

class Glass<T> {
	/*
	 * Milk milk; Water water;
	 */
	T liquid;
}

public class Generics1 {
	public static void main(String[] args) {
		Glass<Milk> glass1 = new Glass<Milk>();
		glass1.liquid = new Milk();
		Milk l = glass1.liquid;

		Glass<Water> glass2 = new Glass<Water>();
		glass2.liquid = new Water();

		Water w = glass2.liquid;

	}
}
