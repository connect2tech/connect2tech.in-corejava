package in.connect2tech.sep19.wknd.ist0900.session3;

class Car {
	
	public void startEngine() {
		System.out.println("startEngine");
	}
}

class Maruti extends Car{
	
}

class Audi extends Car{
	public void startEngine() {
		System.out.println("startEngine of Audi");
	}
}

public class Inheritance {
	public static void main(String str[]) {
		
		Car audi = new Audi();
		audi.startEngine();
		
		
	}
}
