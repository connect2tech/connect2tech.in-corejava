package in.connect2tech.sep19.wknd.ist0900.session2;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class IfElse {
	public static void main(String str[]) {
		int age = 18;
		
		char c1 = 'x';
		char c2 = 'y';
		
		
		if(age>=18) {
			System.out.println("You can cast your vote!!!");
		}else {
			System.out.print("Please wait for your Turn!!!");
		}
		
		if(c1 == 'a') {
			System.out.println("c1 is a");
		}else if(c2 == 'b') {
			
		}else {
			
		}
	}
}
