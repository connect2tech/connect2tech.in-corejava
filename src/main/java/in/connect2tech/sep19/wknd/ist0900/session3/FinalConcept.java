package in.connect2tech.sep19.wknd.ist0900.session3;

class Bike{
	String color;
	final int fuelCapacity=10;
	
	void startEngine() {
		System.out.println("startEngine");
	}
}

class MyBike extends Bike{
	void startEngine() {
		System.out.println("startEngine myBike");
	}
}


public class FinalConcept {
	public static void main(String str[]) {
		Bike b1 = new Bike();
		b1.color ="black";
		
		b1.color = "blue";
		
		//b1.fuelCapacity = 15;
	}
}
