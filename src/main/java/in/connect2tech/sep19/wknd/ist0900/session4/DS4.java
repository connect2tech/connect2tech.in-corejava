package in.connect2tech.sep19.wknd.ist0900.session4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class DS4 {
	public static void main(String str[]) {
		HashMap<String, String> map = new HashMap<String,String>();
		
		map.put("K1", "V1");
		map.put("K2", "V2");
		System.out.println(map);
		
		String val1 = map.get("K1");
		
		Set <String> set = map.keySet();
		
		Iterator <String> iter = set.iterator();
		
		while(iter.hasNext()) {
			String key = iter.next();
			String value = map.get(key);
			
			System.out.println(key);
			System.out.println(value);
		}
	}
}
