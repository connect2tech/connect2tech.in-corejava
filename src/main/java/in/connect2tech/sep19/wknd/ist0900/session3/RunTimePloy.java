package in.connect2tech.sep19.wknd.ist0900.session3;

interface Browser{
	public void open();
}

class Chrome implements Browser{

	public void open() {
		System.out.println("Chrome");
	}
	
}

class FF implements Browser{

	public void open() {
		System.out.println("FF");
	}
	
}

public class RunTimePloy {
	public static void main(String str[]) {

		int choice = 1;
		Browser b;
		
		if(choice == 1) {
			b = new Chrome();
			
		}else {
			b = new FF();
		}
		
		b.open();
	}
}
