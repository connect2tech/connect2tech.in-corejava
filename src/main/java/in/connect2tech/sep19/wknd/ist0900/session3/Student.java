package in.connect2tech.sep19.wknd.ist0900.session3;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Student {
	
	int age;
	String name;
	static String instructor;
	
	
	Student(){
		//System.out.print("I am constructor");
	}
	
	public void study() {
		System.out.print("Study");
		System.out.print(instructor);
		System.out.print(name);
	}
	
	public static void work() {
		System.out.print("work");
		System.out.print(instructor);
		//System.out.print(name);
	}

}
