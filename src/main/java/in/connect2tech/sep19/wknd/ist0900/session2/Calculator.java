package in.connect2tech.sep19.wknd.ist0900.session2;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Calculator {
	public static void main(String str[]) {

		int a = 10;
		int b = 20;
		int c = 20;

		add(a, b);
		add(a, b, c);
		add("java","selenium");
		
		System.out.println(a);

	}

	public static void add(int x, int y) {

		x = x + 10;
		y = y + 20;

		System.out.print(x + y);
	}
	
	public static void add(String  x, String y) {


		System.out.print(x + y);
	}

	public static void add(int x, int y, int z) {

		x = x + 10;
		y = y + 20;

		System.out.print(x + y + z);
	}

}
