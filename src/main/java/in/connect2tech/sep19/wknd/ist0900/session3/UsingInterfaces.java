package in.connect2tech.sep19.wknd.ist0900.session3;

interface RBI {
	void debit();

	abstract void credit();
}

class HDFC implements RBI {

	public void debit() {
		System.out.println("debit");
	}

	public void credit() {
		System.out.println("credit");
	}

}

public class UsingInterfaces {
	public static void main(String str[]) {
		RBI r = new HDFC();
		r.debit();
	}
}
