package in.connect2tech.sep19.wknd.ist0900.session3;


class Person{
	String name;
	
	public void display(String name) {
		System.out.println("display::"+this);
		System.out.println("display::"+this.name);
		
		name = name;
		this.name = name;
	}
}


public class ThisKeyword {
	public static void main(String str[]) {
		Person p1 = new Person();
		p1.name="abc";
		System.out.println(p1);
		
		p1.display("java");
		
		/*Person p2 = new Person();
		System.out.println(p2);*/
		
	}
}
