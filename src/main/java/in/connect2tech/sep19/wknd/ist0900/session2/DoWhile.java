package in.connect2tech.sep19.wknd.ist0900.session2;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class DoWhile {
	public static void main(String str[]) {

		int a = 0;

		do {
			System.out.println("a=" + a);
			--a;
		} while (a > 0);

	}
}
