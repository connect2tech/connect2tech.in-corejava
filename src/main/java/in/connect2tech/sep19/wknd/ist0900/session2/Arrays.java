package in.connect2tech.sep19.wknd.ist0900.session2;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Arrays {
	public static void main(String str[]) {
		int a [] = new int [5];
		int [] b  = {5,1,2,1,5};
		
		for(int i=0;i<5;i++) {
			a[i] = i+10;
		}
		
		for(int i=0;i<5;i++) {
			System.out.println(a[i]); 
		}
	}
}
