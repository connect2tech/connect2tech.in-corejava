package in.connect2tech.sep19.wknd.ist0900.session4;

public class ExceptionHandling {
	public static void main(String str[]) {
		
		int a [] = {10,20};
		
		
		
		try {
			System.out.println("a="+a[2]);	
		}catch(ArithmeticException e) {
			System.out.println("Inside Catch1...");
		}catch(Exception e) {
			System.out.println("Inside Catch2...");
		}
		
		System.out.println("Program Done!!!");
		
	}
}
