package in.connect2tech.sep19.wknd.ist0900.session3;

abstract class Furniture{
	
}

abstract class Shape{
	abstract void draw();
	void color() {
		
	}
}

class Circle extends Shape{
	void draw() {
		System.out.println("draw circle");
	}
}


public class AbstractClass {
	public static void main(String str[]) {

			Shape s = new Circle();
			s.draw();
		
	}
}
