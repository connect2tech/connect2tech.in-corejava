package in.connect2tech.sep19.wknd.ist0900.session3;

public class MainClass {
	public static void main(String str[]) {
		Student s1 = new Student();
		s1.name="Java";
		s1.instructor = "NC";
		
		
		Student s2 = new Student();
		s2.name="Selenium";
		System.out.println(s2.instructor);
		
		s2.instructor="nkc";
		
		System.out.println(s1.instructor);
		
		System.out.println(Student.instructor);
		
		Student.work();
		s1.work();
		s2.work(); 
	}
}
