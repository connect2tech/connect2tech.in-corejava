package in.connect2tech.sep19.wknd.ist0900.session2;

import jdk.internal.dynalink.beans.StaticClass;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Scope {

	static int a = 10;
	
	public static void main(String str[]) {
		int b=20;
		System.out.println(a);
		System.out.println(b);
	}

	public static void m2() {
		System.out.println(a);
		//System.out.println(b);
	}

}
