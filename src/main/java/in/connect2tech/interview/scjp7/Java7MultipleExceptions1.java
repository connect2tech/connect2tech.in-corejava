package in.connect2tech.interview.scjp7;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Java7MultipleExceptions1 {

	public static void main(String[] args) {
		try {
			FileInputStream fis = new FileInputStream(new File(""));
		} catch (IOException | RuntimeException e) {

		}
	}

}