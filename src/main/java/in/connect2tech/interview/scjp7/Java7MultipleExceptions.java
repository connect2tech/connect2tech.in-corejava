package in.connect2tech.interview.scjp7;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class Java7MultipleExceptions {

	public static void main(String[] args) {
		try {
			rethrow("abc");
		} catch (FirstException | SecondException | ThirdException e) {
			// below assignment will throw compile time exception since e is
			// final
			// e = new Exception();
			System.out.println(e.getMessage());
			System.out.println(e);
		}
	}

	static void rethrow(String s) throws FirstException, SecondException, ThirdException {
		try {
			if (s.equals("First"))
				throw new FirstException("First");
			else if (s.equals("Second"))
				throw new SecondException("Second");
			else
				throw new ThirdException("Third");
		} catch (Exception e) {
			// below assignment disables the improved rethrow exception type
			// checking feature of Java 7
			// e=new ThirdException("");
			throw e;
		}
	}

}

class FirstException extends Exception {

	public FirstException(String msg) {
		super(msg);
	}
}

class SecondException extends Exception {

	public SecondException(String msg) {
		super(msg);
	}
}

class ThirdException extends Exception {

	public ThirdException(String msg) {
		super(msg);
	}
}