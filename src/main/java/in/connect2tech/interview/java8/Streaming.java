package in.connect2tech.interview.java8;

import java.util.ArrayList;
import java.util.List;

public class Streaming {
	public static void main(String[] args) {
		List<String> items = new ArrayList<>();
		items.add("A");
		items.add("B");
		items.add("C");
		items.add("D");
		items.add("E");

		items.stream()
		.filter(val -> val.startsWith("A"))
		.forEach(val -> System.out.println(val));
	}
}
