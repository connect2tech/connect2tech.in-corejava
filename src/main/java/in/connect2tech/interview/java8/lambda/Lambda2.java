package in.connect2tech.interview.java8.lambda;

interface Shape {
	void shape(String shape);
}

public class Lambda2 {
	public static void main(String[] args) {

		Shape s1 = (s) -> {
			System.out.println("Drawing :: " + s);
		};

		s1.shape("Circle");
	}

}
