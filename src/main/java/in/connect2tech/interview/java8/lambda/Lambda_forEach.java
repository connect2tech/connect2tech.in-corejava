package in.connect2tech.interview.java8.lambda;

import java.util.ArrayList;
import java.util.List;

public class Lambda_forEach {
	public static void main(String[] args) {

		List<String> items = new ArrayList<>();
		items.add("A");
		items.add("B");
		items.add("C");
		items.add("D");
		items.add("E");

		for (String string : items) {
			System.out.println(string);
		}

		//Internal loop
		items.forEach(val ->System.out.println(val));
	}

}
