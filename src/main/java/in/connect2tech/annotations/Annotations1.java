package in.connect2tech.annotations;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

//Marker Annotation
//Single value annotation
//Multi value annotation

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface SmartPhone{
	String os() default "Win7";
	int version() default 1;
}

@SmartPhone(os="win",version=2)
class NokiaPhones{
	String model;
	int size;
	
	NokiaPhones(String s, int x){
		model = s;
		size = x;
	}
}

public class Annotations1 {
	public static void main(String str[]) {
		NokiaPhones phones = new NokiaPhones("Model1", 5);
		
		Class c = phones.getClass();
		Annotation annotation = c.getAnnotation(SmartPhone.class);
		SmartPhone sPhone = (SmartPhone)annotation;
		System.out.print(sPhone.os());
		
	}
}
