
public class Calculator {
	
	public static void add(int a, int b){
		System.out.println(a+b);
	}
	
	public static void add(String s1, String s2){
		System.out.println(s1 + s2);
	}
	
	public static void add(int a, int b, int c){
		System.out.println(a+b+c);
	}
	
	public static void main(String[] args) {
		add(10,20);
		add(10,20,30);
		add("java", "selenium");
	}
	
}
