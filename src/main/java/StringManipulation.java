
/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class StringManipulation {
	public static void main(String[] args) {
		String str = "HELLO world";
		
		int len = str.length();
		System.out.println("len="+len);
		
		//charAt(int index) 
		//concat(String str)
		//equals(Object anObject) 
		
		char c = str.charAt(0);
		System.out.println(c);
		
		String newString = str.concat(" welcome to java");
		System.out.println(newString);
		
		boolean b =str.equals("hello world");
		System.out.println(b);
		
		String lower = newString.toLowerCase();
		System.out.println(lower);
		
	}
}
