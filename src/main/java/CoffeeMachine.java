
public class CoffeeMachine {

	public static void powerOn(){
		System.out.println("Warm water");
		System.out.println("Warm milk");
		System.out.println("Grind coffee beans");
	}
	
	public static String getCoffee(){
		System.out.println("Add water");
		System.out.println("Add sugar");
		System.out.println("Add coffee");
		return "coffee";
	}
	
	public static String getCustomCoffee(int sugar, int water, int coffee){
		
		System.out.println("Add water.."+water);
		System.out.println("Add sugar.."+sugar);
		System.out.println("Add coffee.."+coffee);
		return "coffee";
		
	}
		
	
	
	public static void main(String[] args) {
		powerOn();
		String mug1 =getCoffee();
		String mug2 = getCoffee();
		String mug3 = getCustomCoffee(3,4,5);

		System.out.println(mug1);
		System.out.println(mug2);
	}

}
