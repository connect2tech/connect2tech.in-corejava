
/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class MathsCalculator {
	
	static int sum(int a, int b){
		int summation = a + b;
		System.out.println(summation);
		return summation;
	}
	
	static String sum(String a, String b){
		String summation = a + b;
		System.out.println(summation);
		return summation;
	}
	
	
	static int sum(int a, int b, int c){
		int summation = a + b + c;
		System.out.println(summation);
		return summation;
	}
	
	
	public static void main(String[] args) {
		int val = sum(10,20);
		int val2 = sum(10,20,30);
		
		String s = sum("java","j2ee");
	}
}
