
/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : http://c2t.nchaurasia.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

abstract class Shape{
	abstract public void draw();
	
	public void color(){
		System.out.println("The color is red...");
	}
}

class Circle extends Shape{
	public void draw(){
		System.out.println("I am circle...");
	}
}



public class AbstractConcepts {
	public static void main(String[] args) {
		Shape s = new Circle();
		s.draw();
	}
}
