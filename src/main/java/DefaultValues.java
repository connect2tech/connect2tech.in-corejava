
public class DefaultValues {
	
	static int a;
	static float b;
	static double c;
	static boolean bool;
	
	public static void main(String[] args) {
		System.out.println("a="+a);
		System.out.println("b="+b);
		System.out.println("c="+c);
		System.out.println("bool="+bool);
		
		//local variable needs to be initialized before they can 
		//be used
		int aa=10;
		
		System.out.println("aa="+aa);
		
	}
	
	public static void method1(){
		
	}
	
}
